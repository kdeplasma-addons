# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2014-10-28 10:19+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "दर्शनीयता"

#: package/contents/config/config.qml:19
#, fuzzy, kde-format
#| msgid "Predefined Timers"
msgctxt "@title"
msgid "Predefined Timers"
msgstr "पूर्व-पारिभाषित टाइमर"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:135
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "टाइमर"

#: package/contents/ui/CompactRepresentation.qml:135
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Start Timer"
msgstr "टाइमर"

#: package/contents/ui/CompactRepresentation.qml:198
#: package/contents/ui/CompactRepresentation.qml:216
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr ""

#: package/contents/ui/configAdvanced.qml:26
#, fuzzy, kde-format
#| msgid "Run a command:"
msgctxt "@option:check"
msgid "Execute command:"
msgstr "आदेश चालवा :"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:35
#, fuzzy, kde-format
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show title:"
msgstr "शिर्षक दर्शवा :"

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgctxt "@option:check"
msgid "Show remaining time"
msgstr ""

#: package/contents/ui/configAppearance.qml:58
#, fuzzy, kde-format
#| msgid "Hide seconds"
msgctxt "@option:check"
msgid "Show seconds"
msgstr "सेकंद लपवा"

#: package/contents/ui/configAppearance.qml:63
#, fuzzy, kde-format
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show timer toggle"
msgstr "शिर्षक दर्शवा :"

#: package/contents/ui/configAppearance.qml:68
#, kde-format
msgctxt "@option:check"
msgid "Show progress bar"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr ""

#: package/contents/ui/configTimes.qml:71
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""

#: package/contents/ui/configTimes.qml:78
#, kde-format
msgid "Add"
msgstr ""

#: package/contents/ui/configTimes.qml:119
#, kde-format
msgid "Scroll over digits to change time"
msgstr ""

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr ""

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr ""

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:67
#, kde-format
msgid "%1 is running"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "%1 not running"
msgstr ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgid "Timer"
msgstr "टाइमर"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Timer finished"
msgstr ""

#: package/contents/ui/main.qml:130
#, fuzzy, kde-format
#| msgid "&Start"
msgctxt "@action"
msgid "&Start"
msgstr "सुरु करा (&S)"

#: package/contents/ui/main.qml:135
#, fuzzy, kde-format
#| msgid "S&top"
msgctxt "@action"
msgid "S&top"
msgstr "बंद करा (&T)"

#: package/contents/ui/main.qml:140
#, fuzzy, kde-format
#| msgid "&Reset"
msgctxt "@action"
msgid "&Reset"
msgstr "पुन्हस्थापित करा (&R)"

#, fuzzy
#~| msgid "Run a command:"
#~ msgctxt "@title:group"
#~ msgid "Run Command"
#~ msgstr "आदेश चालवा :"

#, fuzzy
#~| msgid "Run a command:"
#~ msgctxt "@label:textbox"
#~ msgid "Command:"
#~ msgstr "आदेश चालवा :"

#~ msgctxt "separator of hours:minutes:seconds in timer strings"
#~ msgid ":"
#~ msgstr ":"

#~ msgid "Timer Configuration"
#~ msgstr "टाइमर संयोजना"

#~ msgid "Stop"
#~ msgstr "थांबा"

#~ msgid "Timer Timeout"
#~ msgstr "टाइमर वेळेचे बंधन"

#~ msgid "General"
#~ msgstr "सामान्य"

#~ msgid "Plasma Timer Applet"
#~ msgstr "प्लाज्मा टाइमर एप्लेट"

#~ msgid "Actions on Timeout"
#~ msgstr "वेळ संपल्यावर क्रिया"

#~ msgid "Show a message:"
#~ msgstr "संदेश दर्शवा :"
